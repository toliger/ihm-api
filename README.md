# IHM API

## Install

Pour lancer les services il faut **docker** et **docker-compose**.

## Start

```
docker-compose up
```

## Documentation

You can generate documentation with:

```
cd app && npm i && npm run doc
```

Now, you can open the docs folder with your favorite web navigator
