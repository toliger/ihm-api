const db = require("../db.js");

module.exports = async (gameUID, uuid) => {
  let res = {};

  res.data = null;
  res.error = null;

  const game = await db.getJSON(gameUID);

  if (game.length == 0) {
    res.error = "Game not found";
    return res;
  }

  if (!game.round.includes(uuid)) {
    if (game.round.length < 3) game.round.push(uuid);
    else if (game.round.length == 4) {
      game.round = [];
    }
  }
  
  if (game.round.length == 3) {
    game.isPlaying == "blue" ? "red" : "blue";
    game.needsToGuess = false;
  }
  
  await db.setJSON(gameUID, game);

  res.data = game.round.length;

  return res;
};
