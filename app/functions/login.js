const db = require('../db.js');
const error = require('../utils/error');

module.exports = (uuid) => {
    let res = {};
    res.data = null;
    res.error = null;

    
    return new Promise((resolve, reject) => {
	if(!uuid) {
	    res.error = new error(1, "Missing param");
	    return resolve(res);
	}

	db.get(uuid).then((user) => {
	    if(!user) {
		res.error = new error(2, "user doesnt exist");
		return resolve(res);
	    }
	    
	    user = JSON.parse(user);
	    res.data = {};
	    res.data.name = user.name;
	    res.data.uuid = user.uuid;
	    resolve(res);
	}).catch((error) => {
	    console.error(error);
	    reject("db error");
	});
    });
}
