const db = require("../db.js");
const idgen = require("./utils.js").uuid;
const error = require("../utils/error");

module.exports = async (playerUUID, r) => {
    let res = {};
    res.data = null;
    res.error = null;

    if (!playerUUID || typeof r == "undefined") {
      res.error = new error(1, "Missing param");
      return res;
    }

    let player = await db.getJSON(playerUUID);
    const s = await db.setJSON(
      playerUUID,
      {
        name: player.name,
        team: player.team,
        ready: r
      }
    );
    res.data = {};
    res.data.uuid = playerUUID;
    res.data.name = player.name;
    return res;
};
