const fr_words = require("../words/french.json");
const en_words = require("../words/english.json");

function random(a, b) {
  return Math.floor(Math.random() * b + a);
}

module.exports = (lang) => {
  let grid = [];

  if (lang == '🇬🇧') words = en_words;
  else words = fr_words;

  for (let i = 0; i < 25; i++)
    grid.push({ name: words[i], show: false, team: null });

  let blue = 0;
  let red = 0;
  let black = 0;
  let r = 0;
  while (blue != 9) {
    r = random(0, 24);
    if (grid[r].team == null) {
      grid[r].team = "blue";
      blue++;
    }
  }
  while (red != 8) {
    r = random(0, 24);
    if (grid[r].team == null) {
      grid[r].team = "red";
      red++;
    }
  }
  while (black != 1) {
    r = random(0, 24);
    if (grid[r].team == null) {
      grid[r].team = "black";
      black++;
    }
  }
  return grid;
};
