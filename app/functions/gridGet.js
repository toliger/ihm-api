const db = require("../db.js");
const error = require("../utils/error");
const player = require("./player.js");

module.exports = async uuid => {
  let res = {};
  res.data = null;
  res.error = null;

  if (!uuid) {
    res.error = new error(1, "Missing param");
    return res;
  }

  const game = await db.getJSON(uuid);

  if (!game) {
    res.error = new error(2, "game doesnt exist");
    return res;
  }

  res.data = {};
  game.words = game.words.map(x => {
    delete x.show;
    return x;
  });

  res.data.words = await Promise.all(game.words);

  return res;
};
