const newGrid = require("./newGrid.js");
const db = require("../db.js");
const player = require("./player.js");

function random(a, b) {
  return Math.floor(Math.random() * b + a);
}

module.exports = async lobbyUUID => {
  const lob = await db.getJSON(lobbyUUID);

  if (lob.created) return false;

  lob.ended = false;
  lob.blackPick = false;
  lob.words = newGrid(lob.lang);
  if (lob.ended) delete lob.ended;

  let red_spy = [],
    blue_spy = [];

  let team_blue = [],
    team_red = [];

  // Get players
  await Promise.all(
    lob.players.map(async uuid => {
      let p = await player(uuid);
      p.data.ready = false;
      p.data.role = "agent";
      if (p.data.team == "blue") team_blue.push(uuid);
      else team_red.push(uuid);
      db.setJSON(uuid, p.data);
    })
  );

  // Define agents
  while (red_spy.length == 0 || blue_spy.length == 0) {
    if (red_spy.length == 0)
      red_spy.push(team_red[random(0, team_red.length - 1)]);

    if (blue_spy.length == 0)
      blue_spy.push(team_blue[random(0, team_blue.length - 1)]);
  }

  // Set agents role in db
  await Promise.all(
    red_spy.map(async uuid => {
      let data = await player(uuid);
      data.data.role = "spy";
      db.setJSON(uuid, data.data);
    })
  );

  await Promise.all(
    blue_spy.map(async uuid => {
      let data = await player(uuid);
      data.data.role = "spy";
      db.setJSON(uuid, data.data);
    })
  );

  lob.isPlaying = "blue";
  // needsToGuess:
  // est fausse si l'agent doit envoyer un mot
  // est vraie si le spy doit deviner les cases
  lob.needsToGuess = false;
  lob.created = true;
  lob.numberToGuess = 1;
  lob.score = {};
  lob.score.blue = 0;
  lob.score.red = 0;
  lob.leaving = null;
  lob.state = 1;

  const s = await db.setJSON(lobbyUUID, lob);

  return true;
};
