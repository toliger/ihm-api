const db = require("../db.js");
const error = require("../utils/error");

module.exports = async lobbyUUID => {
  const lob = await db.getJSON(lobbyUUID);
  const players = await Promise.all(lob.players.map(pId => db.getJSON(pId)));
  let blue_team = 0,
    red_team = 0;
  players.map(p => {
    if (p.team == "blue") blue_team++;
    else red_team++;
  });
  return players.length < 4 || red_team < 2 || blue_team < 2
    ? false
    : players.every(p => p.ready);
};
