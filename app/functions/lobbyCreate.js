const db = require("../db.js");
const idgen = require("./utils.js").uuid;

module.exports = async (name, password, difficulty, lang) => {
  let res = {};
  res.data = null;
  res.error = null;
  const uuid = idgen();
  const id = uuid.slice(0, 5);
  const round = [];
  const players = [];
  if (name == "") name = "lobby";
  const s = await db.setJSON(uuid, {
    name,
    id,
    password,
    difficulty,
    players,
    round,
    state: 0,
    lang
  });
  let lobbs = await db.getJSON("lobbies");

  if (!lobbs || lobbs == "1" || lobbs === "undefined") {
    lobbs = [];
    lobbs.push(uuid);
  } else {
    lobbs = lobbs.concat(uuid);
  }
  const se = await db.setJSON("lobbies", lobbs);
  res.lobbs = {};
  res.lobbs.uuid = uuid;
  res.lobbs.name = name;
  res.lobbs.state = 0;
  res.lobbs.players = players;
  res.lobbs.lang = lang;
  res.lobbs.round = round;

  return res;
};
