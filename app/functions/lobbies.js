const db = require("../db.js");
const lobby = require("./lobby");

module.exports = async () => {
  let res = {};
  res.data = null;
  res.error = null;

  let lobs = await db.getJSON("lobbies");

  if (!lobs || lobs === "undefined") {
    db.setJSON("lobbies", []);
    lobs = [];
  }

  if (lobs == "[]") {
    res.data = [];
    return res;
  }

  lobs = lobs.map(async function(e) {
    let lob = await lobby(e);
    return lob.data;
  });

  let lobbs = await Promise.all(lobs);

  lobbs.filter(e => e.state == 0);

  res.data = lobbs;

  return res;
};
