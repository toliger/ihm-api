const db = require('../db.js');
const idgen = require('./utils.js').uuid;
const error = require('../utils/error');

module.exports = name => {
  return new Promise((resolve, reject) => {
    let res = {};
    res.data = null;
    res.error = null;

    if (!name) {
      res.error = new error(1, 'Missing param');
      return resolve(res);
    }

    const uuid = idgen();
    db.set(
      uuid,
      JSON.stringify({
        name: name,
        ready: false,
        team: 1,
      }),
    )
      .then(() => {
        res.data = {};
        res.data.uuid = uuid;
        res.data.name = name;
        resolve(res);
      })
      .catch(err => {
        console.error(err);
        reject('Error adding uuid & name to db.');
      });
  });
};
