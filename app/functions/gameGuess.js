const db = require("../db.js");
const player = require("./player.js");

module.exports = async (game, player, word, number) => {
  let res = {};
  res.data = null;
  res.error = null;

  const gameData = await db.getJSON(game);
  const playerData = await db.getJSON(player);

  if (
    gameData.isPlaying == playerData.team &&
    !gameData.needsToGuess &&
    playerData.role == "agent"
  ) {
    if (number > 0 && number < 9 && (word != "" && word.length < 40)) {
      res.data = {};
      res.data.number = number;
      res.data.word = word;
      gameData.numberToGuess = number;
      gameData.needsToGuess = true;
      gameData.wordToGuess = word;
      const s = await db.setJSON(game, gameData);
      return res;
    } else {
      res.error =
        "The word you give has to have a length between 1 and 40 included.";
      return res;
    }
    res.error = "The number of cards to be chosen is between 1 and 8 included.";
    return res;
  } else {
    res.error = "You are not in a position to prompt a word.";
    return res;
  }
};
