const db = require("../db.js");

module.exports = async (playerUUID, name) => {
  let res = {};
  res.data = null;
  res.error = null;

  const p = await db.getJSON(playerUUID);

  if (p.length == 0) {
    res.error = "Player not found";
  }

  p.name = name;

  await db.setJSON(playerUUID, p);

  res.data = p;
  res.data.uuid = playerUUID;
  return res;
};
