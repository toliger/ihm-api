const db = require("../db.js");
const error = require("../utils/error");
const player = require("./player.js");
const lobbyLeave = require("./lobbyLeave.js");

module.exports = async (uuid, playerUUID) => {
  let res = {};
  res.data = null;
  res.error = null;

  if (!uuid) {
    res.error = new error(1, "Missing param");
    return res;
  }

  const game = await db.getJSON(uuid);
  const player = await db.getJSON(playerUUID);

  if (!game || !player) {
    res.error = new error(2, "game or player could not be found in db");
    return res;
  }

  res.data = "Player left";
  game.leaving = player.name;

  for (let i in game.players) await lobbyLeave(game.players[i], uuid);
  game.players = [];
  game.ended = true;
  game.winner = game.isPlaying == "red" ? "blue" : "red";

  const s = await db.setJSON(uuid, game);

  return res;
};
