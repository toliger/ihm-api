const db = require("../db.js");
const error = require("../utils/error");
const player = require("./player.js");

module.exports = async uuid => {
  let res = {};
  res.data = null;
  res.error = null;

  if (!uuid) {
    res.error = new error(1, "Missing param");
    return res;
  }

  const game = await db.getJSON(uuid);
  if (!game) {
    res.error = new error(2, "game doesnt exist");
    return res;
  }

  res.data = {};
  res.data.uuid = uuid;
  res.data.name = game.name;
  res.data.id = game.id;
  res.data.isPlaying = game.isPlaying;
  res.data.created = game.created;
  res.data.needsToGuess = game.needsToGuess;
  res.data.wordToGuess = game.wordToGuess;
  res.data.numberToGuess = game.numberToGuess;
  res.data.score = {};
  res.data.score.blue = game.score.blue;
  res.data.score.red = game.score.red;
  res.data.round = game.round;
  res.data.winner = game.winner;

  if (game.ended) res.data.ended = true;
  if (game.leaving) res.data.leaving = game.leaving;
  if (game.blackPick) res.data.blackPick = true;

  game.players = game.players.map(uuid => {
    return player(uuid)
      .then(data => {
        return data.data;
      })
      .catch(err => {
        console.error(err);
        return null;
      });
  });

  game.words = game.words.map(x => {
    if (!x.show) {
      x.team = null;
      delete x.show;
    }
    return x;
  });

  res.data.words = await Promise.all(game.words);
  res.data.players = await Promise.all(game.players);

  return res;
};
