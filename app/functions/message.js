const player = require("./player.js");
let wordfilter = require("wordfilter");
const french_bw = require("../blackWords/french.json");
const english_bw = require("../blackWords/english.json");
const deutsh_bw = require("../blackWords/deutsh.json");

module.exports = async (playerUUID, lobbyUUID, msg) => {
  let res = {};
  res.data = null;
  res.error = null;

  res.data = {};
  if (playerUUID != "server") {
    const user = await player(playerUUID);
    res.data.from = user.data.name;
  } else res.data.from = "server";

  wordfilter.addWords(french_bw);
  wordfilter.addWords(english_bw);
  wordfilter.addWords(deutsh_bw);

  if (wordfilter.blacklisted(msg)) msg = "****";

  res.data.text = msg;

  return res;
};
