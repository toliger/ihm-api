const db = require("../db.js");
const player = require("./player.js");
const game = require("./game.js");
const lobbyLeave = require("./lobbyLeave.js");

module.exports = async (gameUUID, playerUUID, cardID) => {
  let res = {};
  res.data = null;
  res.error = null;

  if (cardID < 0 && cardID > 24) {
    res.data = {};
    res.data.status = false;
    return res;
  }

  let game = await db.getJSON(gameUUID);
  let player = await db.getJSON(playerUUID);

  if (game.words[cardID].show) {
    res.data = {};
    res.data.status = false;
    return res;
  }

  if (
    game.isPlaying == player.team &&
    game.needsToGuess &&
    player.role == "spy"
  ) {
    game.words[cardID].show = true;

    if (game.numberToGuess > 0) {
      switch (game.words[cardID].team) {
        case "black": {
          game.blackPick = true;
          game.ended = true;
          for (let i in game.players)
            await lobbyLeave(game.players[i], gameUUID);
          game.players = [];
          game.winner = game.isPlaying == "red" ? "blue" : "red";
          break;
        }

        case "blue": {
          game.score.blue++;
          break;
        }

        case "red": {
          game.score.red++;
          break;
        }
      }
      game.numberToGuess--;
      if (!game.numberToGuess) {
        game.isPlaying = game.isPlaying == "blue" ? "red" : "blue";
        game.needsToGuess = false;
      }
      if (game.score.blue == 9 || game.score.red == 8) {
        for (let i in game.players) await lobbyLeave(game.players[i], gameUUID);
        game.players = [];
        game.ended = true;
        game.winner = game.score.blue == 9 ? "blue" : "red";
      }
    }
    const s = await db.setJSON(gameUUID, game);
  }

  res.data = {};
  res.data.status = true;
  return res;
};
