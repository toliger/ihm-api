const db = require('../db.js');
const lobbies = require('./lobbies.js');
const lobbyDelete = require('./lobbyDelete.js');

module.exports = (call, callback) => {
  new Promise((resolve, reject) => {
    lobbies({}, (err, res) => {
      res = JSON.parse(res.res);
      if (res.error) return reject(res.error);

      for (let i = 0; i < res.data.length; i++) {
        lobbyDelete({request: {uuid: `${res.data[i]}`}}, (err, res) => {
          res = JSON.parse(res.res);
          if (err) return reject('cant delete a lobby');
        });
      }
      db.set('lobbies', '[]')
        .then(() => {
          resolve('lobbies deleted');
        })
        .catch(err => {
          reject('error');
        });
    });
  })
    .then(data => {
      data = JSON.stringify(data);
      callback(null, {res: `{ "data": ${data}, "error": null}`});
    })
    .catch(error => {
      error = JSON.stringify(error);
      callback(null, {res: `{ "data": null, "error": ${error}}`});
    });
};
