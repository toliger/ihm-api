const db = require("../db.js");
const lobby = require("./lobby.js");
const player = require("./player.js");
const error = require("../utils/error");

module.exports = (userUUID, lobbyUUID) => {
  return new Promise((resolve, reject) => {
    let res = {};
    res.data = null;
    res.error = null;

    if (!lobbyUUID || !userUUID) {
      res.error = new error(1, "Missing Param");
      return resolve(res);
    }
    db.get(lobbyUUID)
      .then(lobby => {
        lobby = JSON.parse(lobby);
        if (lobby.error) {
          res.error = new error(2, "Lobby not found");
          resolve(res);
        }
        player(userUUID)
          .then(player => {
            if (player.error) {
              reject(player.error);
            }
            player = player.data;
            if (lobby.players.includes(player.uuid)) {
              res.error = new error(3, "Player already in lobby");
              return reject(res.error);
            }
            lobby.players.push(player.uuid);

            db.set(
              lobbyUUID,
              JSON.stringify({
                name: lobby.name,
                players: lobby.players,
                id: lobby.id,
                difficulty: lobby.difficulty,
                password: lobby.password,
                state: 0,
                lang: lobby.lang,
                round: lobby.round
              })
            )
              .then(() => {
                res.data = lobby;
                resolve(res);
              })
              .catch(err => {
                res.error = new error(4, "Could not add player to lobby");
                reject(res);
              });
          })
          .catch(err => {
            reject(new error(2, "player not found"));
          });
      })
      .catch(err => {
        reject(new error(2, "lobby not found"));
      });
  });
};
