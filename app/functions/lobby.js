const db = require("../db.js");
const error = require("../utils/error");
const player = require("./player.js");

module.exports = async uuid => {
  let res = {};
  res.data = null;
  res.error = null;

  if (!uuid) {
    res.error = new error(1, "Missing param");
    return res;
  }

  let lobby = await db.getJSON(uuid);

  if (!lobby) {
    res.error = new error(2, "lobby doesnt exist");
    return res;
  }
  res.data = {};
  res.data.uuid = uuid;
  res.data.name = lobby.name;
  res.data.id = lobby.id;
  res.data.lock = lobby.password == "" ? false : true;
  res.data.pwd = lobby.password;
  res.data.state = lobby.state;
  res.data.lang = lobby.lang;
  res.data.round = lobby.round;
  res.data.players_uuid = lobby.players;

  let resa = await Promise.all(
    lobby.players.map(async x =>
      (await player(x)).data
    )
  );

  res.data.players = resa;
  return res;
};
