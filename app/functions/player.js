const db = require("../db.js");
const Error = require("../utils/error");

module.exports = async playerUUID => {
  let res = {};
  res.data = null;
  res.error = null;

  const p = await db.getJSON(playerUUID);
  res.data = p;
  res.data.uuid = playerUUID;
  return res;
};
