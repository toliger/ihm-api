const db = require('../db.js');
const lobbies = require('./lobbies.js');
const error = require('../utils/error');

module.exports = uuid => {
  return new Promise((resolve, reject) => {
    let res = {};
    res.data = null;
    res.error = null;

    if (!uuid) {
      res.error = new error(1, 'Missing param');
      return resolve(res);
    }

    db.del(uuid)
      .then(d => {
        if (!d) {
          res.error = new error(2, 'lobby cant be removed, it doesnt exist');
          return resolve(res);
        }

        res.data = {};
        res.data.message = 'lobby deleted';

        lobbies().then(r => {
          r = JSON.parse(r);

          if (!r || err || r.error) {
            res.error = new error(3, 'lobby doesnt exist');
            return reject(res);
          }

          if (r.data.length == 1) {
            db.set('lobbies', '[]')
              .then(() => {
                resolve('lobbies register cleaned');
              })
              .catch(err => {
                console.error(err);
                reject('could not clean lobbies register');
              });
          } else {
            let p = r.data.indexOf(call.request.uuid);
            r = r.data.splice(p, 1);
            db.set('lobbies', r)
              .then(() => {
                resolve('lobby removed from register');
              })
              .catch(err => {
                console.error(err);
                reject('Couldnt remove lobby from register');
              });
          }
        });

        resolve(data);
      })
      .catch(error => {
        console.error(error);
        reject('cannot delete lobby, perhaps it doesnt exist');
      });
  });
};
