const db = require("../db.js");
const idgen = require("./utils.js").uuid;
const error = require("../utils/error");

module.exports = async (playerUUID, t) => {
  let res = {};
  res.data = null;
  res.error = null;

  if (!playerUUID || typeof t == "undefined") {
    res.error = new error(1, "Missing param");
    return res;
  }

  let player = await db.getJSON(playerUUID);
  if (!player.ready) player.team = t;
  const s = await db.setJSON(playerUUID, player);
};
