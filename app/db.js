let redis = require("redis");
let bluebird = require("bluebird");
const bdd_conf = require("./config/redis.json");

// ========== DB ===========

// Create Promises for redis
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let connection = redis.createClient(bdd_conf.port, bdd_conf.host);

module.exports.get = key => {
  return connection.getAsync(key);
};

module.exports.getJSON = async key => {
  const data = await module.exports.get(key);
  return JSON.parse(data);
};

module.exports.set = (key, value) => {
  return connection.setAsync(key, value);
};

module.exports.setJSON = (key, value) => {
  return module.exports.set(key, JSON.stringify(value));
};

module.exports.del = (key, value) => {
  return connection.delAsync(key);
};
