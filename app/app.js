const app = require("express")();
const server = require("http").Server(app);
const io = require("socket.io")(server);

const ping = require("./functions/ping");

const lobby = require("./functions/lobby");
const lobbyJoin = require("./functions/lobbyJoin");
const lobbyLeave = require("./functions/lobbyLeave");
const lobbyCreate = require("./functions/lobbyCreate");
const lobbyDelete = require("./functions/lobbyDelete");
const lobbies = require("./functions/lobbies");

const player = require("./functions/player");
const playerChangeName = require("./functions/playerChangeName");
const playerReady = require("./functions/playerReady");
const checkLobbyReady = require("./functions/checkLobbyReady");
const playerTeam = require("./functions/playerTeam");
const login = require("./functions/login");
const register = require("./functions/register");

const checkCard = require("./functions/checkCard.js");

const game = require("./functions/game");
const gameCreate = require("./functions/gameCreate");
const gameGuess = require("./functions/gameGuess");
const gameLeave = require("./functions/gameLeave");

const gridGet = require("./functions/gridGet");

const message = require("./functions/message");
const round = require("./functions/round");

const CONF = require("./config/app.json");

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

server.listen(8081);

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", function(socket) {
  /**
   * Ping
   * @name Ping
   * @returns {String} Pong
   */
  socket.on("ping", function() {
    ping()
      .then(res => {
        socket.emit("ping", res);
      })
      .catch(error => {
        console.error("ping failed !");
      });
  });

  /**
   * Remove player from a lobby/game
   * @name gameLeave
   * @param {String} gameUUID The game UID
   * @param {String} playerUUID The UUID of the player
   * @return {String} Completion message or Error message
   * @return {websocket} Broadcast the new game to everyone
   */
  socket.on("gameLeave", async function(gameUUID, playerUUID) {
    gameLeave(gameUUID, playerUUID)
      .then(async res => {
        const g = await game(gameUUID);
        socket.emit("game", g);
        socket.broadcast.to(gameUUID).emit("game", g);
      })
      .catch(error => {
        console.error(error);
      });
  });

  socket.on("gameGuess", function(game, player, word, number) {
    gameGuess(game, player, word, number)
      .then(data => {
        if (data.error) {
          socket.emit("gameGuess", data);
        } else {
          socket.broadcast.to(game).emit("gameGuess", data);
          socket.emit("gameGuess", data);
        }
      })
      .catch(err => {
        console.error(err);
      });
  });

  /**
   * Check a card
   * @name checkCard
   * @param {String} gameUUID lobby/game UUID
   * @param {String} playerUUID player UUID
   * @param {String} cardID card id
   * @return {String} ::TODO
   * @return {websocket} Broadcast the new game to this game
   */
  socket.on("checkCard", function(gameUUID, playerUUID, cardID) {
    checkCard(gameUUID, playerUUID, cardID)
      .then(async res => {
        socket.emit("checkCard", res);
        const g = await game(gameUUID);
        socket.emit("game", g);
        socket.in(gameUUID).emit("game", g);
      })
      .catch(error => {
        console.error(error);
      });
  });

  /**
   * Get a lobby
   * @name lobby
   * @param {String} lobbyUUID lobby UUID
   * @return {String} lobby
   */
  socket.on("lobby", function(lobbyUUID) {
    lobby(lobbyUUID)
      .then(res => {
        socket.emit("lobby", res);
      })
      .catch(error => {
        console.error("lobby failed !", error);
      });
  });

  /**
   * Leave a lobby
   * @name lobbyLeave
   * @param {String} lobbyUUID lobby UUID
   * @param {String} playerUUID player UUID
   * @return {Array} The updated lobby
   * @return {websocket} Broadcast the new lobby list
   */
  socket.on("lobbyLeave", function(userUUID, lobbyUUID) {
    lobbyLeave(userUUID, lobbyUUID)
      .then(async res => {
        socket.emit("lobbyLeave", res);
        const lobbs = await lobbies();
        const lobb = await lobby(lobbyUUID);
        socket.in("lobbies").emit("lobbies", lobbs);
        socket.in(lobbyUUID).emit("lobbyLeave", lobb);
      })
      .catch(error => {
        console.error("lobbyLeave failed !", error);
        socket.emit("lobbyLeave", { data: null, error: error });
      });
  });

  /**
   * Join a lobby
   * @name lobbyJoin
   * @param {String} lobbyUUID lobby UUID
   * @param {String} playerUUID player UUID
   * @return {Array} The updated lobby
   * @return {websocket} Broadcast the new lobby list
   */
  socket.on("lobbyJoin", function(playerUUID, lobbyUUID) {
    socket.join(lobbyUUID);
    message("server", lobbyUUID, "new_player")
      .then(m => {
        socket.in(lobbyUUID).emit("message", m);
        socket.emit("message", m);
      })
      .catch(error => {
        console.error("message failed !");
      });
    lobbyJoin(playerUUID, lobbyUUID)
      .then(res => {
        socket.emit("lobbyJoin", res);
        lobbies()
          .then(res => {
            socket.in("lobbies").emit("lobbies", res);
          })
          .catch(error => {
            console.error("lobbies failed !", error);
          });
      })
      .catch(error => {
        console.error("lobbyJoin failed !", error);
        socket.emit("lobbyJoin", { data: null, error: error });
      });
  });

  /**
   * Create a new lobby
   * @name lobbyCreate
   * @param {String} name The name of the lobby
   * @param {String} password The password used to enter the lobby
   * @param {String} difficulty The difficulty of the words that will be
   *	    prompted in the game
   * @return {Array} The new lobby with set parameters
   * @return {websocket} Broadcasting all lobbies with the new one added
   */
  socket.on("lobbyCreate", function(
    name = "no name",
    password = "",
    difficulty = 3,
    lang = "fr"
  ) {
    lobbyCreate(name, password, difficulty, lang)
      .then(res => {
        socket.emit("lobbyCreate", res);
        lobbies()
          .then(res => {
            socket.emit("lobbies", res);
            socket.in("lobbies").emit("lobbies", res);
          })
          .catch(error => {
            console.error("lobbies failed !", error);
          });
      })
      .catch(error => {
        console.error("lobbyCreate failed !");
      });
  });

  /**
   * Delete a new lobby
   * @name lobbyDelete
   * @param {String} uuid The UID of the lobby to delete
   * @return {String} Deletion message (error or not) TODO
   * @return {websocket} Broadcasting all lobbies (updated)
   */
  socket.on("lobbyDelete", function(uid) {
    lobbyDelete(uid)
      .then(res => {
        socket.emit("lobbyDelete", res);
        lobbies()
          .then(res => {
            socket.in("lobbies").emit("lobbies", res);
          })
          .catch(error => {
            console.error("lobbies failed !", error);
          });
      })
      .catch(error => {
        console.error("lobbyDelete failed !");
      });
    lobbies()
      .then(res => {
        socket.in("lobbies").emit("lobbies", res);
      })
      .catch(error => {
        console.error("lobbies failed !", error);
      });
  });

  /**
   * List all lobbies in the database register
   * @name lobbies
   * @return {Array} All lobbies retrieved from the database
   * @return {websocket} Broadcasting all lobbies
   */
  socket.on("lobbies", function() {
    socket.join("lobbies");
    lobbies()
      .then(res => {
        socket.emit("lobbies", res);
      })
      .catch(error => {
        console.error("lobbies failed !", error);
      });
  });

  /**
     * Game creation after every player is ready
     * @name gameCreate
     * @param {String} gameUID The UID of the lobby that is evolving to
	    become a game
     * @return {Array} The new game created
     */
  socket.on("gameCreate", function(gameUUID) {
    gameCreate(gameUUID)
      .then(res => {})
      .catch(error => {
        console.error("gameCreate failed !", error);
      });
  });

  /**
   * Retrieve informations about the game
   * @name game
   * @param {String} gameUID The game UID
   * @return {Array} The game with all informations about it including players
   * @return {websocket} Broadcasting the array to the person that asked for it
   */
  socket.on("game", function(gameUID) {
    socket.join(gameUID);
    game(gameUID)
      .then(res => {
        socket.emit("game", res);
      })
      .catch(error => {
        console.error("game failed !", error);
        socket.emit("game", error);
      });
  });

  /**
   * Create a new grid of words for a new game
   * @name gridGet
   * @param {String} gameUID The UID of the game to put the words in
   * @return {Array} The words that were created or error if any.
   */
  socket.on("gridGet", function(gameUID) {
    gridGet(gameUID)
      .then(res => {
        socket.emit("gridGet", res);
      })
      .catch(error => {
        console.error("grid Get failed !", error);
        socket.emit("grid Get", error);
      });
  });

  /**
   * Get all informations about a player
   * @name player
   * @param {String} playerUUID self-explanatory
   * @return {Array} All informations about the player includin: UUID, name, the team he's in, ..
   * @return {websocket} Broadcasting the array to the person that asked for it
   */
  socket.on("player", function(playerUUID) {
    player(playerUUID)
      .then(res => {
        socket.emit("player", res);
      })
      .catch(error => {
        console.error("player failed !", error);
        socket.emit("player", error);
      });
  });

  /**
   * Change a player's name
   * @name playerChangeName
   * @param {String} playerUUID self-explanatory
   * @param {String} name new name he wishes to have
   * @return {Array} All informations about the player includin: UUID, name, the team he's in, ..
   * @return {websocket} Broadcasting the array to the person that asked for it
   */
  socket.on("playerChangeName", function(playerUUID, name) {
    playerChangeName(playerUUID, name)
      .then(res => {
        socket.emit("playerChangeName", res);
      })
      .catch(error => {
        console.error("player failed !", error);
        socket.emit("playerChangeName", error);
      });
  });
  /**
   * Change current ready-state of player within a lobby
   * @name playerReady
   * @param {String} playerUUID self-explanatory
   * @param {String} lobbyUUID self-explanatory
   * @param {Boolean} ready The state of the player
   * @return {Array} Player infos
   * @return {websocket} Broadcasting player's ready-state to all people in the lobby
   */
  socket.on("playerReady", function(playerUUID, lobbyUUID, ready) {
    playerReady(playerUUID, ready)
      .then(res => {
        socket.emit("playerReady", res);
        lobby(lobbyUUID)
          .then(res => {
            socket.emit("lobby", res);
            socket.in(lobbyUUID).emit("lobby", res);
          })
          .catch(error => console.error("playerReady failed!", error));
      })
      .catch(error => {
        console.error("playerReady failed !", error);
      });
    checkLobbyReady(lobbyUUID).then(r => {
      if (r) {
        socket.emit("counter", CONF.game.counter);
        socket.in(lobbyUUID).emit("counter", CONF.game.counter);
      }
    });
  });

  /**
     * Change a player's team
     * @name playerTeam
     * @param {String} playerUUID self-explanatory
     * @param {String} lobbyUID self-explanatory
     * @param {String} team The team the player wants to be on.
	It can be either blue or red.
     * @return {websocket} Broadcasting the team the player is in to all players in lobby
     */
  socket.on("playerTeam", function(playerUID, lobbyUID, team) {
    playerTeam(playerUID, team)
      .then(res => {
        lobby(lobbyUID)
          .then(res => {
            socket.emit("lobby", res);
            socket.in(lobbyUID).emit("lobby", res);
          })
          .catch(error => console.error("playerReady failed!", error));

        socket.emit("playerTeam", res);
      })
      .catch(error => {
        console.error("playerTeam failed !", error);
      });
  });

  /**
   * login to the game
   * @name login
   * @param {String} uuid The player's uuid
   * @return {Array} Player's name and uuid for confirmation
   * @return {websocket} Giving the player his name and the possibility to play.
   */
  socket.on("login", function(uuid) {
    login(uuid)
      .then(res => {
        socket.emit("login", res);
      })
      .catch(error => {
        console.error("login failed !");
      });
  });

  /**
   * Register to the game
   * @name register
   * @param {String} name The name you want to use within the game
   * @return {Array} Your uuid is returned as well as your name for confirmation
   * @return {websocket} Your uuid and name is given to you
   */
  socket.on("register", function(name) {
    register(name)
      .then(res => {
        socket.emit("register", res);
      })
      .catch(error => {
        console.error("register failed !");
      });
  });

  /*
  socket.on("round", function(gameUID, uuid) {
    round(gameUID, uuid)
      .then(res => {
        game(gameUID)
          .then(res => {
            socket.emit("game", res);
          })
          .catch(error => {
            console.error("game failed !", error);
            socket.emit("game", error);
          });
      })
      .catch(err => {
        console.error(err);
      });
  });
  */

  /**
   * Send messages within a lobby
   * @name message
   * @param {String} uuid self-explanatory
   * @param {String} lobbyUID self-explanatory
   * @param {String} msg The message to send
   * @return {Array} Message and lobby
   * @return {websocket} Broadcasting the message to all players in the lobby
   */
  socket.on("message", function(playerUUID, lobbyUUID, msg) {
    message(playerUUID, lobbyUUID, msg)
      .then(m => {
        socket.in(lobbyUUID).emit("message", m);
        socket.emit("message", m);
      })
      .catch(error => {
        console.error("message failed !", error);
      });
  });
});
