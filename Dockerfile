FROM keymetrics/pm2:latest-alpine

ADD app /app

WORKDIR /app

RUN npm i

CMD ["pm2-docker", "app.js"]

EXPOSE 8081
